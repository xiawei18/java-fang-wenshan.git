package com.zking.shiro;

import com.zking.ssm.biz.ClazzBiz;
import com.zking.ssm.model.Clazz;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-10-26 15:29
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class ClazzBizTest {
    @Autowired
    private ClazzBiz clazzBiz;

    @Test
    public void test1(){
        System.out.println(clazzBiz.selectByPrimaryKey(13));
        System.out.println(clazzBiz.selectByPrimaryKey(13));
    }

    @Test
    public void test2(){
        clazzBiz.deleteByPrimaryKey(10);
    }

    @Test
    public void test3(){
        Clazz clz=new Clazz(13,"定时刷新","哈哈哈哈","/upload/mvc/特工队第一课.png");
        clazzBiz.updateByPrimaryKeySelective(clz);
    }
}
