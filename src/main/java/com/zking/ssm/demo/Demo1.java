package com.zking.ssm.demo;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-09-18 23:18
 */
public class Demo1 {

    public int add(String s){
        System.out.println("新增一条数据");
        return 1;
    }

    public int edit(String s){
        System.out.println("修改一条数据");
        return 1;
    }
}
