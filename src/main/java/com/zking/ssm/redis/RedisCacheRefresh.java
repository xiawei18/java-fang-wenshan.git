package com.zking.ssm.redis;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Java方文山
 * @compay csdn_Java方文山
 * @create 2023-11-07-9:02
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RedisCacheRefresh {

    /**
     * 缓存key
     */
    String value();

    /**
     * 刷新周期（单位：秒）
     */
    int refreshInterval() default 60;

}
