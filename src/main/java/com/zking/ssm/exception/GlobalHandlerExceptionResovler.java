package com.zking.ssm.exception;

import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-22 15:28
 *
 * 处理全局异常的解析器
 */
//@Component
public class GlobalHandlerExceptionResovler implements HandlerExceptionResolver {
    /**
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @param o 目标对象
     * @param e 目标对象执行，出现的异常对象
     * @return
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest,
                                         HttpServletResponse httpServletResponse,
                                         Object o, Exception e) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("error");
        if (e instanceof GlobalException){
            GlobalException exception = (GlobalException) e;
            mv.addObject("ex",exception.getMessage());
            mv.addObject("msg","全局异常,错误码501");
        }else if(e instanceof RuntimeException){
            RuntimeException exception = (RuntimeException) e;
            mv.addObject("ex",exception.getMessage());
            mv.addObject("msg","运行时异常,错误码601");
        }
        return mv;
    }
}
