package com.zking.ssm.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-22 15:39
 *
 * 当出现异常，统一向前端响应错误信息的json对象数据
 */
@RestControllerAdvice
public class GlobalExceptionResolver2 {

    @ExceptionHandler
    public Map handler(Exception e){
        Map map = new HashMap();
        if (e instanceof GlobalException){
            GlobalException exception = (GlobalException) e;
            map.put("ex",exception.getMessage());
            map.put("msg","全局异常 GlobalExceptionResolver,错误码501");
        }else if(e instanceof RuntimeException){
            RuntimeException exception = (RuntimeException) e;
            map.put("ex",exception.getMessage());
            map.put("msg","运行时异常 GlobalExceptionResolver,错误码601");
        }
        return map;
    }

}


