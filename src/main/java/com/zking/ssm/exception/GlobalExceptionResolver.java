package com.zking.ssm.exception;

import org.springframework.web.servlet.ModelAndView;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-22 15:39
 */
//@ControllerAdvice
public class GlobalExceptionResolver {

//    @ExceptionHandler
    public ModelAndView handler(Exception e){
        ModelAndView mv = new ModelAndView();
        mv.setViewName("error");
        if (e instanceof GlobalException){
            GlobalException exception = (GlobalException) e;
            mv.addObject("ex",exception.getMessage());
            mv.addObject("msg","全局异常 GlobalExceptionResolver,错误码501");
        }else if(e instanceof RuntimeException){
            RuntimeException exception = (RuntimeException) e;
            mv.addObject("ex",exception.getMessage());
            mv.addObject("msg","运行时异常 GlobalExceptionResolver,错误码601");
        }
        return mv;
    }

}


