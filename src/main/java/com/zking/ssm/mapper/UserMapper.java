package com.zking.ssm.mapper;

import com.zking.ssm.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface UserMapper {
    int deleteByPrimaryKey(Integer userid);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userid);

//  通过用户名 查询 对应的 角色
    Set<String> selectRoleIdsByUserName(@Param("userName") String userName);

//    通过 账户名 查询 对应的 权限
    Set<String> selectPerIdsByUserName(@Param("userName") String userName);

//    通过 账户名 查询用户信息
    User queryUserByUserName(@Param("userName") String userName);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}