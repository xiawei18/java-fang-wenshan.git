package com.zking.ssm.shiro;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-27 9:01
 */
public class MySessionListener implements SessionListener {
    @Override
    public void onStart(Session session) {
        System.out.println("MySessionListener.onStart 执行..."+session.getId());
    }

    @Override
    public void onStop(Session session) {
        System.out.println("MySessionListener.onStop 执行..."+session.getId());

    }

    @Override
    public void onExpiration(Session session) {
        System.out.println("MySessionListener.onExpiration 执行..."+session.getId());
    }
}
