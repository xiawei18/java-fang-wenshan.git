package com.zking.ssm.shiro;


/**
 * 演示利用缓存存储数据
 * @author Administrator
 *
 */
public class EhcacheDemo2 {
	public static void main(String[] args) {
		System.out.println(System.getProperty("java.io.tmpdir"));
//		EhcacheUtil.put("com.javaxl.four.entity.Book", 11, "zhangsan");
//		System.out.println(EhcacheUtil.get("com.javaxl.four.entity.Book", 11));

		EhcacheUtil.put("com.javaxl.one.entity.User", 11, "zhangsan");
		System.out.println(EhcacheUtil.get("com.javaxl.one.entity.User", 11));

	}
}
