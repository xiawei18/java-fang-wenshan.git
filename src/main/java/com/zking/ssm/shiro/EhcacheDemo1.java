package com.zking.ssm.shiro;

import java.util.HashMap;
import java.util.Map;

/**
 * 利用map集合简易实现缓存原理
 * @author Administrator
 *
 */
public class EhcacheDemo1 {
	static Map<String, Object> cache = new HashMap<String, Object>();
//	看做是查询数据库 userid =1
	static Object getValue(String key) {
//		默认从缓存中获取数据
		Object value = cache.get(key);
		if(value == null) {
//			从数据库中做查询 userBiz.queryRole(1);
			System.out.println("hello zs");
			cache.put(key, new String[] {"zs"});
			return cache.get(key);
		}
		return value;
	}
	
	public static void main(String[] args) {
		System.out.println(getValue("sname"));
		System.out.println(getValue("sname"));
	}
}
