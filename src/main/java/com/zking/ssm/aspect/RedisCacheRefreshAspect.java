package com.zking.ssm.aspect;

import com.zking.ssm.redis.RedisCacheRefresh;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;

/**
 * @author Java方文山
 * @compay csdn_Java方文山
 * @create 2023-11-07-11:14
 */

@Aspect
@Component
public class RedisCacheRefreshAspect {
    @Autowired
    private RedisTemplate redisTemplate;

    @Pointcut("@annotation(RedisCacheRefresh)")
    public void cacheRefreshPointcut(RedisCacheRefresh RedisCacheRefresh) {
    }

    @Around("cacheRefreshPointcut(RedisCacheRefresh)")
    public Object doCacheRefresh(ProceedingJoinPoint joinPoint, RedisCacheRefresh RedisCacheRefresh) throws Throwable {
        System.out.println("进入切面");
        String key = RedisCacheRefresh.value();
        long refreshInterval = RedisCacheRefresh.refreshInterval();

        if (redisTemplate.hasKey(key)) {
            System.out.println("有缓存");
            // 如果缓存存在，则延迟刷新
            redisTemplate.expire(key, refreshInterval, TimeUnit.SECONDS);
        }

        // 执行原始方法
        return joinPoint.proceed();
    }
}
