package com.zking.ssm.controller;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-26 16:09
 */
@RequestMapping("/shiro")
@Controller
public class ShiroController {

//    RequiresUser代表 当前方法只有登录后才能够访问
//    RequiresUser 等价于 spring-shiro.xml中的/user/updatePwd.jsp=authc配置
    @RequiresUser
    @RequestMapping("/passUser")
    public String passUser(){
        System.out.println("身份认证通过...");
        return "admin/addUser";
    }

    //    RequiresRoles 当前方法只有 具备指定的角色 才能够访问、
    //    RequiresRoles 等价于 spring-shiro.xml中的/admin/*.jsp=roles[4]配置
    @RequiresRoles(value = {"1","4"},logical = Logical.OR)
    @RequestMapping("/passRole")
    public String passRole(){
        System.out.println("角色认证通过...");
        return "admin/addUser";
    }

    //    RequiresPermissions 当前方法只有 具备指定的权限 才能够访问、
    //    RequiresPermissions 等价于 spring-shiro.xml中的/user/teacher.jsp=perms[2]配置
    @RequiresPermissions(value = {"2"},logical = Logical.AND)
    @RequestMapping("/passPermission")
    public String passPermission(){
        System.out.println("权限认证通过...");
        return "admin/addUser";
    }

}
