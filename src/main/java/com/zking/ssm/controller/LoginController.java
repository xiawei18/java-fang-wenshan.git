package com.zking.ssm.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-19 10:37
 */
@Controller
public class LoginController {

//    @RequestMapping("/login")
//    public String login(HttpServletRequest request){
////        登录成功 需要 保存 用户信息
//        String uname = request.getParameter("uname");
//        if("zhangsan".equals(uname)){
//            request.getSession().setAttribute("uname",uname);
//        }
//        return "index";
//    }
//
//    @RequestMapping("/logout")
//    public String logout(HttpServletRequest request){
//        request.getSession().invalidate();
//        return "index";
//    }


    @RequestMapping("/login")
    public String login(HttpServletRequest request){
        try {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            UsernamePasswordToken token = new UsernamePasswordToken(username,password);
            Subject subject = SecurityUtils.getSubject();
            subject.login(token);
            return "main";
        }catch (Exception e){
            request.setAttribute("message","账号密码错误...");
            return "login";
        }
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "login";
    }
}
