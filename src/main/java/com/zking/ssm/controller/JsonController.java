package com.zking.ssm.controller;

import com.zking.ssm.biz.ClazzBiz;
import com.zking.ssm.exception.GlobalException;
import com.zking.ssm.model.Clazz;
import com.zking.ssm.util.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-22 14:30
 *
 */
@RestController
@RequestMapping("/clz/json")
public class JsonController {
    @Autowired
    private ClazzBiz clazzBiz;

    @RequestMapping("/clzEdit")
    public String clzEdit() {
        System.out.println("JsonController.clzEdit");
        return "clzEdit";
    }


    //    List<T>
    @RequestMapping("/list")
    public List<Clazz> list(HttpServletRequest request, Clazz clazz) {
        PageBean pageBean = new PageBean();
        pageBean.setRequest(request);
        if(true)
            throw new RuntimeException("查询班级信息异常，异常存在于JsonController.list。。。。");
//        [{},{}]
        return this.clazzBiz.listPager(clazz, pageBean);
    }


    //    List<Map>
    @RequestMapping("/listMap")
    public List<Map> listMap(HttpServletRequest request, Clazz clazz) {
        PageBean pageBean = new PageBean();
        pageBean.setRequest(request);
//        [{},{}]
        return this.clazzBiz.listMapPager(clazz, pageBean);
    }

    //    Map
    @RequestMapping("/map")
    public Map map(HttpServletRequest request, Clazz clazz) {
        PageBean pageBean = new PageBean();
        pageBean.setRequest(request);
//        {}
        return this.clazzBiz.listMapPager(clazz, pageBean).get(0);
    }


    //    T
    @RequestMapping("/load")
    public Clazz load(HttpServletRequest request, Clazz clazz) {
//        http://localhost:8080/clz/json/load?cid=2
        PageBean pageBean = new PageBean();
        pageBean.setRequest(request);
        if(true)
            throw new GlobalException("系统繁忙，请参考E007;");

//        {}
        return this.clazzBiz.listPager(clazz, pageBean).get(0);
    }


    //    {
    //	msg:"",
    //	code:200,
    //	data:[],
//        pageBean:{}
    //}
    @RequestMapping("/hunhe")
    public Map hunhe(HttpServletRequest request, Clazz clazz) {
//        http://localhost:8080/clz/json/load?cid=2
        PageBean pageBean = new PageBean();
        pageBean.setRequest(request);
        List<Clazz> lst = this.clazzBiz.listPager(clazz, pageBean);
        Map map = new HashMap();
        map.put("data", lst);
        map.put("pagebean", pageBean);
        return map;
    }


}
