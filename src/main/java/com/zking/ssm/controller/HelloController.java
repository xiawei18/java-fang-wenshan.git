package com.zking.ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-19 10:03
 */
@Controller
public class HelloController {

    @RequestMapping("hello")
    public String hello(){
        System.out.println("进入业务方法...");
        return "index";
    }
}
