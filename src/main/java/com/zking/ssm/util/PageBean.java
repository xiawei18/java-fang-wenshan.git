package com.zking.ssm.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 分页工具类
 *
 */
public class PageBean {

	private int page = 1;// 页码

	private int rows = 10;// 页大小

	private int total = 0;// 总记录数

	private boolean pagination = true;// 是否分页
	
	private String url; //保存上一次请求的URL
	
	private Map<String,String[]> paramMap = new HashMap<>();// 保存上一次请求的参数
	
	/**
	 * 初始化pagebean的，保存上一次请求的重要参数
	 * @param req
	 */
	public void setRequest(HttpServletRequest req) {
//		1.1	需要保存上一次请求的URL
		this.setUrl(req.getRequestURL().toString());
//		1.2	需要保存上一次请求的参数	bname、price
		this.setParamMap(req.getParameterMap());
//		1.3	需要保存上一次请求的分页设置	pagination
		this.setPagination(req.getParameter("pagination"));
//		1.4	需要保存上一次请求的展示条目数
		this.setRows(req.getParameter("rows"));
//		1.5  初始化请求的页码	page
		this.setPage(req.getParameter("page"));
	}
	
	public void setPage(String page) {
		if(StringUtils.isNotBlank(page))
			this.setPage(Integer.valueOf(page));
	}

	public void setRows(String rows) {
		if(StringUtils.isNotBlank(rows))
			this.setRows(Integer.valueOf(rows));
	}

	public void setPagination(String pagination) {
//		只有在前台jsp填写了pagination=false，才代表不分页
		if(StringUtils.isNotBlank(pagination))
			this.setPagination(!"false".equals(pagination));
	}


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Map<String, String[]> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, String[]> paramMap) {
		this.paramMap = paramMap;
	}



	public PageBean() {
		super();
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setTotal(String total) {
		this.total = Integer.parseInt(total);
	}

	public boolean isPagination() {
		return pagination;
	}

	public void setPagination(boolean pagination) {
		this.pagination = pagination;
	}

	/**
	 * 获得起始记录的下标
	 * 
	 * @return
	 */
	public int getStartIndex() {
		return (this.page - 1) * this.rows;
	}
	
	/**
	 * 最大页
	 * @return
	 */
	public int maxPage() {
//		total % rows == 0 ? total / rows : total / rows +1
		return this.total % this.rows == 0 ? this.total / this.rows : this.total / this.rows + 1;
	}
	
	/**
	 * 下一页
	 * @return
	 */
	public int nextPage() {
//		如果当前页小于最大页，那就下一页为当前页+1；如果不小于，说明当前页就是最大页，那就无需+1
		return this.page < this.maxPage() ? this.page + 1 : this.page;
	}
	
	/**
	 * 上一页
	 * @return
	 */
	public int previousPage() {
		return this.page > 1 ? this.page - 1 : this.page;
	}

	@Override
	public String toString() {
		return "PageBean [page=" + page + ", rows=" + rows + ", total=" + total + ", pagination=" + pagination + "]";
	}

}
