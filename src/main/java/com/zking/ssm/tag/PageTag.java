package com.zking.ssm.tag;

import com.zking.ssm.util.PageBean;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


public class PageTag extends BodyTagSupport{
	private PageBean pageBean;// 包含了所有分页相关的元素
	
	public PageBean getPageBean() {
		return pageBean;
	}

	public void setPageBean(PageBean pageBean) {
		this.pageBean = pageBean;
	}

	@Override
	public int doStartTag() throws JspException {
//		没有标签体，要输出内容
		JspWriter out = pageContext.getOut();
		try {
			out.print(toHTML());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return super.doStartTag();
	}

	private String toHTML() {
		StringBuffer sb = new StringBuffer();
//		隐藏的form表单---这个就是上一次请求下次重新发的奥义所在
//		上一次请求的URL
		sb.append("<form action='"+pageBean.getUrl()+"' id='pageBeanForm' method='post'>");
		sb.append("	<input type='hidden' name='page'>");
//		上一次请求的参数
		Map<String, String[]> paramMap = pageBean.getParamMap();
		if(paramMap != null && paramMap.size() > 0) {
			Set<Entry<String, String[]>> entrySet = paramMap.entrySet();
			for (Entry<String, String[]> entry : entrySet) {
//				参数名
				String key = entry.getKey();
//				参数值
				for (String value : entry.getValue()) {
//					上一次请求的参数，再一次组装成了新的Form表单
//					注意：page参数每次都会提交，我们需要避免
					if(!"page".equals(key)) {
						sb.append("	<input type='hidden' name='"+key+"' value='"+value+"' >");
					}
				}
			}
		}
		sb.append("</form>");
		
//		分页条
		sb.append("<ul class='pagination justify-content-center'>");
		sb.append("	<li class='page-item "+(pageBean.getPage() == 1 ? "disabled" : "")+"'><a class='page-link'");
		sb.append("	href='javascript:gotoPage(1)'>首页</a></li>");
		sb.append("	<li class='page-item "+(pageBean.getPage() == 1 ? "disabled" : "")+"'><a class='page-link'");
		sb.append("	href='javascript:gotoPage("+pageBean.previousPage()+")'>&lt;</a></li>");// less than 小于号
//		sb.append("	<li class='page-item'><a class='page-link' href='#'>1</a></li>");
//		sb.append("	<li class='page-item'><a class='page-link' href='#'>2</a></li>");
		sb.append("	<li class='page-item active'><a class='page-link' href='#'>"+pageBean.getPage()+"</a></li>");
		sb.append("	<li class='page-item "+(pageBean.getPage() == pageBean.maxPage() ? "disabled" : "")+"'><a class='page-link' href='javascript:gotoPage("+pageBean.nextPage()+")'>&gt;</a></li>");
		sb.append("	<li class='page-item "+(pageBean.getPage() == pageBean.maxPage() ? "disabled" : "")+"'><a class='page-link' href='javascript:gotoPage("+pageBean.maxPage()+")'>尾页</a></li>");
		sb.append("	<li class='page-item go-input'><b>到第</b><input class='page-link'");
		sb.append("	type='text' id='skipPage' name='' /><b>页</b></li>");
		sb.append("	<li class='page-item go'><a class='page-link'");
		sb.append("	href='javascript:skipPage()'>确定</a></li>");
		sb.append("	<li class='page-item'><b>共"+pageBean.getTotal()+"条</b></li>");
		sb.append("</ul>");
		
//		分页执行的JS代码
		sb.append("<script type='text/javascript'>");
		sb.append("	function gotoPage(page) {");
		sb.append("		document.getElementById('pageBeanForm').page.value = page;");
		sb.append("		document.getElementById('pageBeanForm').submit();");
		sb.append("	}");
		sb.append("	function skipPage() {");
		sb.append("		var page = document.getElementById('skipPage').value;");
		sb.append("		if (!page || isNaN(page) || parseInt(page) < 1 || parseInt(page) > "+pageBean.maxPage()+") {");
		sb.append("			alert('请输入1~"+pageBean.maxPage()+"的数字');");
		sb.append("			return;");
		sb.append("		}");
		sb.append("		gotoPage(page);");
		sb.append("	}");
		sb.append("</script>");
		
		return sb.toString();
	}
}
