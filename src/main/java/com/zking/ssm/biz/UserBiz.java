package com.zking.ssm.biz;

import com.zking.ssm.model.User;

import java.util.Set;

public interface UserBiz {
    int deleteByPrimaryKey(Integer userid);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userid);

    User queryUserByUserName(String userName);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    Set<String> selectRoleIdsByUserName(String userName);

    Set<String> selectPerIdsByUserName(String userName);
}