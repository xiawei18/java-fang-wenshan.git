package com.zking.ssm.biz.impl;

import com.zking.ssm.biz.ClazzBiz;
import com.zking.ssm.mapper.ClazzMapper;
import com.zking.ssm.model.Clazz;
import com.zking.ssm.util.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-17 15:13
 */
@Service
public class ClazzBizImpl implements ClazzBiz {
    @Autowired
    private ClazzMapper clazzMapper;
    @Override
    public int deleteByPrimaryKey(Integer cid) {
        System.out.println("不做任何操作...");
//        return clazzMapper.deleteByPrimaryKey(cid);
        return 0;
    }

    @Override
    public int insert(Clazz record) {
        return clazzMapper.insert(record);
    }

    @Override
    public int insertSelective(Clazz record) {
        return clazzMapper.insertSelective(record);
    }

    @Override
    public Clazz selectByPrimaryKey(Integer cid) {
        return clazzMapper.selectByPrimaryKey(cid);
    }

    @Override
    public int updateByPrimaryKeySelective(Clazz record) {
        return clazzMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Clazz record) {
        return clazzMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<Clazz> listPager(Clazz clazz, PageBean pageBean) {
        return clazzMapper.listPager(clazz);
    }

    @Override
    public List<Map> listMapPager(Clazz clazz, PageBean pageBean) {
        if(true)
            throw new RuntimeException("查询班级信息异常，异常存在于ClazzBizImpl.list。。。。");
        return clazzMapper.listMapPager(clazz);
    }
}
