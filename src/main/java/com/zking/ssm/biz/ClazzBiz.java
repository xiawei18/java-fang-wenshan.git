package com.zking.ssm.biz;

import com.zking.ssm.model.Clazz;
import com.zking.ssm.redis.RedisCacheRefresh;
import com.zking.ssm.util.PageBean;
import jdk.internal.org.objectweb.asm.tree.analysis.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Map;

public interface ClazzBiz {
    @CacheEvict(value = "xx",key = "'cid:'+#cid",allEntries = true)
    int deleteByPrimaryKey(Integer cid);

    int insert(Clazz record);

    int insertSelective(Clazz record);

//    xx=cache-cid:1
//    key的作用改变原有的key生成规则
    @Cacheable(value = "xx",key = "'cid:'+#cid",condition = "#cid > 6")
//    @CachePut(value = "xx",key = "'cid:'+#cid")
    Clazz selectByPrimaryKey(Integer cid);

    @RedisCacheRefresh(value="xx",refreshInterval=30)
    int updateByPrimaryKeySelective(Clazz record);

    int updateByPrimaryKey(Clazz record);

    List<Clazz> listPager(Clazz clazz, PageBean pageBean);
    List<Map> listMapPager(Clazz clazz, PageBean pageBean);
}