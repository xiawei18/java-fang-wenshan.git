package com.zking.ssm.model.dto;

import com.zking.ssm.model.Clazz;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author小李飞刀
 * @site www.javaxl.com
 * @company xxx公司
 * @create  2022-08-17 16:15
 */
public class ClazzDto extends Clazz {
//    multipartFile的属性名一定要跟 form 中的表单属性名一致
    private MultipartFile picFile;

    public MultipartFile getPicFile() {
        return picFile;
    }

    public void setPicFile(MultipartFile picFile) {
        this.picFile = picFile;
    }
}
