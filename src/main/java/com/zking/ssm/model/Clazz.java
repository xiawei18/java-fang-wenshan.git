package com.zking.ssm.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * NotNull:针对的是基本数据类型
 *  @NotEmpty 作用于集合
 *  @NotBlank    作用于字符串
 */
public class Clazz {
    @NotNull(message = "cid不能为空")
    protected Integer cid;

    @NotBlank(message = "班级名称不能为空")
    protected String cname;

    @NotBlank(message = "教员老师不能为空")
    protected String cteacher;

    protected String pic;

    public Clazz(Integer cid, String cname, String cteacher, String pic) {
        this.cid = cid;
        this.cname = cname;
        this.cteacher = cteacher;
        this.pic = pic;
    }

    public Clazz() {
        super();
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCteacher() {
        return cteacher;
    }

    public void setCteacher(String cteacher) {
        this.cteacher = cteacher;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}